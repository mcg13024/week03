import java.util.InputMismatchException;
import java.util.Scanner;

public class main{

    public static int divided (int num1, int num2)
            throws ArithmeticException
    {
        return  num1 / num2;
    }
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        boolean continueLoop = true;
        do {
            try
            {

                System.out.println("Please enter a number you want to divide: ");
                int num1 = scanner.nextInt();
                System.out.println("Please enter the number you want to divided by: ");
                int num2 = scanner.nextInt();
                int dividend = num1, divisor = num2;
                int math = dividend / divisor;
                int remainder = dividend % divisor;
                System.out.println("num1: " + num1);
                System.out.println("num2: " + num2);
                System.out.println("The result is " + math + "\nwith a remainder of " + remainder);
                continueLoop = false;
            }catch (InputMismatchException inputMismatchException)
            {
                System.err.println("Exeption: " + inputMismatchException);
                scanner.nextLine();
                System.out.println("Whole numbers only.\nPlease try again.\n");
                continueLoop = true;
            }catch (ArithmeticException arithmeticExpeption)
            {
                System.err.println("Exception: " + arithmeticExpeption);
                System.out.println("Really? This is a simple program for basic math.\nPlease try again. \n\n");;
                continueLoop = true;
            }
        }while (continueLoop);
    }
}







